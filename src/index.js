import React from 'react'
import {render} from 'react-dom'

import Clock from './Components/Level1_Stateful'

render(<Clock />,
       document.getElementById("react-container"));
