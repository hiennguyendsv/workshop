import {render} from 'react-dom'
import {Component} from 'react'
import React from 'react'

export default class Clock extends Component {
  constructor(props) {
    super(props);
    this.myFunc = this.myFunc.bind(this);
  }
  myFunc(){
    console.log("Hello World")
  }
  render() {
    return (
      <div>
        <h1>Hello, world!</h1>
        <h2>It is {new Date().toLocaleTimeString()}.</h2>
        <button onClick = {this.myFunc}> Click me to log message! </button>
      </div>
    );
  }
}
