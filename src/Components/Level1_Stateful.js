import {render} from 'react-dom'
import {Component, PropTypes} from 'react'
import React from 'react'

export default class Clock extends Component {
  render() {
    return (
      <div>
        <h1>Hello, world!</h1>
        <h2>It is {new Date().toLocaleTimeString()}.</h2>
      </div>
    );
  }
}
