import {render} from 'react-dom'
import {Component} from 'react'
import React from 'react'

export default class Clock extends Component {
  constructor(props) {
    super(props);
    this.state = {date: new Date()};
  }
  componentDidMount() {
    this.timerID = setInterval(
      () => { this.setState({date: new Date()}) }
      , 1000 );
  }
  componentWillUnmount() {
    clearInterval(this.timerID);
  }
  myFunc(){
    console.log("Hello World")
  }
  render() {
    return (
      <div>
        <h1>Hello, world!</h1>
        <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
        <button onClick = {this.myFunc}> Click me to log message! </button>
      </div>
    );
  }
}
